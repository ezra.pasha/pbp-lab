from django.shortcuts import render
from .forms import friends
from .models import Friend
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context={}

    if request.method == 'POST':

        form = friends(request.POST)
        
        if form.is_valid():
            form.save()
            return redirect('/lab-3')
            
    else:
        
        form = friends()
        
    context['form']=form
    return render(request, "lab3_form.html", context)
