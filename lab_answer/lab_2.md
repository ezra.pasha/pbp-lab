1. Apakah perbedaan antara JSON dan XML?

- JSON sebagai singkatan dari Javascript Object Notation lebih mudah digunakan dengan Javascript dibandingkan XML
- XML lebih susah dibaca dibandingkan JSON
- XML hanya dapat menggunakan String

Sumber : https://www.guru99.com/json-vs-xml-difference.html

2. Apakah perbedaan antara HTML dan XML?

- HTML bersifat statis sedangkan XML bersifat dinamis
- HTML berguna untuk menampilkan data sementara XML berguna untuk mentransfer data
- XML adalah bahasa pemrograman yang mendefinisikan bahasa pemrograman lain, salah satunya adalah HTML

Sumber : https://www.upgrad.com/blog/html-vs-xml/
