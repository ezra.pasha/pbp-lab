from django.urls import path
from .views import index, createXML, createJSON

urlpatterns = [
    path('', index, name='index'),
    path('xml', createXML, name='xml'),
    path('json',createJSON,name='json')
]
